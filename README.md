# Sorter

## Sort

Sorts photos. Point it a directory `SEL` in a structure such as:

```
.
├── SEL
│   ├── unsorted
│   │    └── Photo.png
│   ├── cat1
│   └── cat2
```

or

```
.
├── Photos
│   ├── SEL
│   │    └── Photo.png
│   ├── cat1
│   └── cat2
```

where your unsorted pictures are next to Photo.png.



## Rename

Originally designed to rename emojis, this was the basis for Sort. CLI-only launch at the moment.